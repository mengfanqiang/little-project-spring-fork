package com.ruyuan.little.project.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForkProjectSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(ForkProjectSpringApplication.class, args);
    }

}
