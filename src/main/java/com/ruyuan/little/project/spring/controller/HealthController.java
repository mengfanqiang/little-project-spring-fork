package com.ruyuan.little.project.spring.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruyuan.little.project.common.dto.CommonResponse;
/**
* @Description:  监控检查
* @Author: Qiuwuzhi
* @CreateDate: 2021/8/5 0005 上午 7:00
* @Version V1.0
*/
@RestController
public class HealthController {

    @RequestMapping(value = "/")
    public CommonResponse health(){
        return CommonResponse.success();
    }
}
